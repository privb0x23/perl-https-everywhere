# HTTPS Everywhere Library and Squid Plugin

## About

An adaptation of Mike Cardwell's HTTPSEverywhere Perl library and squid plugin.

## ChangeLog

2017-08-01 - Modifications to the original files.
* HTTPSEverywhere.pm
  * Fix: Change file paths to work with OPNsense.
  * Fix: Default match target of '0'.
  * Fix: Allow single quotes in the XML, not just double quotes.
  * Add: Use caution and ignore a rule if the platform is mixed content.
* squid.pl
  * Fix: Change file paths to work with OPNsense, including perl executable.

## Licence

[GNU General Public Licence v2.0](LICENCE).

Copyright (c) 2010  Mike Cardwell

Copyright (c) 2017  privb0x23
