#! /usr/bin/env sh

# Copyright (c) 2017  privb0x23

clonedir="/usr/local/libexec/squid/https-everywhere/git/https-everywhere"

# from: https://github.com/dyne/dowse/blob/master/modules/available/squid-httpseverywhere/update_rules.zsh

if [ -r "${clonedir}/.git" ]; then
	cd "${clonedir}" && \
	git checkout . && \
	git pull --rebase && \
	exit 0
else
	# formerly https://git.torproject.org/https-everywhere.git
	git clone 'https://github.com/EFForg/https-everywhere' "${clonedir}" && \
	exit 0 || \
	exit 1
fi

exit 2

# eof
