#!/usr/local/bin/perl
use strict;
use warnings;
use lib '/usr/local/libexec/squid/https-everywhere';
use HTTPSEverywhere;

# from: https://gitlab.com/mikecardwell/perl-HTTPSEverywhere/raw/master/squid.pl
# from: https://github.com/dyne/dowse/raw/master/modules/available/squid-httpseverywhere/conf.zsh

# This is a redirector for Squid. It redirects requests to https according to the
# HTTPS Everywhere rulesets.
#
# Installation instructions:
#
# 1.) In /usr/local/libexec/squid/https-everywhere/ place this file,
#     and HTTPSEverywhere.pm
#
# 2.) Create a directory at /usr/local/libexec/squid/https-everywhere/git,
#     cd into that directory and do a:
#     git clone https://git.torproject.org/https-everywhere.git
#
# 3.) Edit /etc/squid/squid.conf and add this line:
#     redirect_program /usr/local/libexec/squid/https-everywhere/squid.pl
#     url_rewrite_children 5
#
# 4.) Restart squid.
#
# You should cd in to /usr/local/libexec/squid/https-everywhere/git
# and do a "git pull" on a regular basis, to pull down ruleset changes.
# Cron it if possible

$|=1;

my $base = '/usr/local/libexec/squid/https-everywhere/git/https-everywhere';

my $he      = new HTTPSEverywhere( rulesets => ["$base/src/chrome/content/rules"] );
my $updated = time;

while ( <> ) {
	my( $url ) = /^(\S+)/;

	## Re-read the rulesets if it has been more than an hour
	if ( $updated < time-3600 ) {
		$he->read();
		$updated = time;
	}

	## Converted version of the url
	my $newurl = $he->convert( $url );

	if ( $url eq $newurl ) {
		print "$url\n";
	} else {
		print "301:$newurl\n";
	}
}
